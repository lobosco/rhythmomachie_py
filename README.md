README:
=======

rhythmo.py ist ein kleines Python-Script, welches die Rhythmomachie in einer kleinen Textbasierten und grafischen Version auf den Rechner bringt.

Prerequisites:
==============

Python 2.x 

Installation:
=============

git clone

cd

chmod +x rhythmo.py

./rhythmo.py

Usage:
======

-h, --help          displays help message

-l, --landscape     landscape mode in ascii 

-gui, --gui         displays the GUI 

-ai, --ai           enables AI support

Regeln:
=======

siehe pdf im Ordner manual.

Zudem sei auf die Wikipedia verwiesen: https://de.wikipedia.org/wiki/Zahlenkampfspiel#Regeln

Es werden jedoch alle Arten von Grundrechenarten (+,-,*,/) zugelassen.