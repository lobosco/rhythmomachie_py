#!/usr/bin/python

import rhythmo_base as rb
import numpy
import sys

board = numpy.full((8,16), '0.0.0.0', dtype=object) 
#initial pieces
#uneven
board[7, 0] = 'U.4.361.0'
board[7, 1] = 'U.P.190.0'
board[7, 2] = 'U.3.100.0'
board[6, 0] = 'U.4.225.0'
board[6, 1] = 'U.4.120.0'
board[6, 2] = 'U.3.90.0'
board[1, 0] = 'U.4.121.0'
board[1, 1] = 'U.4.66.0'
board[1, 2] = 'U.3.25.0'
board[0, 0] = 'U.4.49.0'
board[0, 1] = 'U.4.28.0'
board[0, 2] = 'U.3.16.0'
board[5, 1] = 'U.3.64.0'
board[5, 2] = 'U.2.81.0'
board[5, 3] = 'U.2.9.0'
board[4, 1] = 'U.3.56.0'
board[4, 2] = 'U.2.49.0'
board[4, 3] = 'U.2.7.0'
board[3, 1] = 'U.3.30.0'
board[3, 2] = 'U.2.25.0'
board[3, 3] = 'U.2.5.0'
board[2, 1] = 'U.3.36.0'
board[2, 2] = 'U.2.9.1'
board[2, 3] = 'U.2.3.0'
#even side
board[0, 15] = 'E.4.289.0'
board[0, 14] = 'E.4.153.0'
board[0, 13] = 'E.3.81.0'
board[1, 15] = 'E.4.169.0'
board[1, 14] = 'E.P.91.0'
board[1, 13] = 'E.3.72.0'
board[6, 15] = 'E.4.81.0'
board[6, 14] = 'E.4.45.0'
board[6, 13] = 'E.3.6.0'
board[7, 15] = 'E.4.25.0'
board[7, 14] = 'E.4.15.0'
board[7, 13] = 'E.3.9.0'
board[2, 14] = 'E.3.49.0'
board[2, 13] = 'E.2.64.0'
board[2, 12] = 'E.2.8.0'
board[3, 14] = 'E.3.42.0'
board[3, 13] = 'E.2.36.0'
board[3, 12] = 'E.2.6.0'
board[4, 14] = 'E.3.20.0'
board[4, 13] = 'E.2.16.0'
board[4, 12] = 'E.2.4.0'
board[5, 14] = 'E.3.25.0'
board[5, 13] = 'E.2.4.1'
board[5, 12] = 'E.2.2.0'

if('-h' in sys.argv or '--help' in sys.argv):
    print("Usage:\n -ai, --ai \t enables AI support \n -h, --help \t displays this message \n -gui, --gui \t displays the GUI \n -log [file] \t enables logging of turns to file\n -l, --landscape \t landscape mode in ascii \n -m [colour-IP-port] \t enables multiplayer game \n -m6 [colour-IP-port] \t enables multiplayer game via IPv6 \n")
else:
   # rb.board = board
   #if multiplayer use mp-play-version
    if('-m' in sys.argv):
        #get colour, adress and port and pass these to play_mp
        i = sys.argv.index('-m')
        argstring = sys.argv[i+1] 
        colour,adress,port = argstring.split('-')
        rb.play_mp(board,colour,adress,port,0)
    if('-m6' in sys.argv):
        #get colour, adress and port and pass these to play_mp
        i = sys.argv.index('-m6')
        argstring = sys.argv[i+1] 
        colour,adress,port = argstring.split('-')
        rb.play_mp(board,colour,adress,port,1)
    else: 
        rb.play(board)



#multiplayer protocol: 
#check, if turn is legit, then send it
#then wait for incoming turn (display popup window during wait)
#execute turn and rebuild
#
#-m[colour:IP:port]
#