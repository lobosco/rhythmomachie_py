#!/usr/bin/python

from __future__ import print_function #needed for print syntax
import numpy
import os #needed for clearing the terminal
import sys #needed for parsing command line arguments
import Tkinter as tk
import tkMessageBox
import multiprocessing as mp
import socket
import thread
#declare global variables
count_even = 0
count_uneven = 0
steps = 0
thisturn = '0.0:0:0.0'
thisrange = 0
thiscolour = 'E'
root = 0
gui = 0
first_turn = 0
list_label = []
list_bg_label = []
why = False #global variable indicating, whan to print what has fallen and why

def repaint_gui(board):
    global list_bg_label
    global list_label
    u = 0
    v = 0
    for i,row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour,piece_type,piece_value_str,piece_count = board[i,j].split('.')
            if(piece_type == 'P' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackpyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackcirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacktriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacksquare.gif').read())
            elif(piece_type == 'P' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitepyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitecirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitetriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitesquare.gif').read())
            else:
                bg_image=tk.PhotoImage(data=open('img/white.gif').read())
            bg_label = list_bg_label[u]
            L = list_label[v]
            u = u+1
            v = v+1
            bg_label.configure(image=bg_image, bg='white')
            bg_label.image=bg_image
            if(piece_value_str != '0'):
                L.configure(text=piece_value_str,anchor='s')
            else:
                L.configure(text='',anchor='s')
    #when the board is drawn display possible turns in terminal
    if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                if(thiscolour == 'E'):
                    turns = getallturns(board, 'E') 
                    print(turns);
                    print(len(turns));
                elif(thiscolour == 'U'):
                    turns = getallturns(board, 'U') 
                    print(turns);
                    print(len(turns));            
    return;
        
def on_click(i,j,event,board):
    global thisturn
    global steps
    global thisrange
    global root
    global thiscolour
    turn_start, turn_range, turn_end = thisturn.split(':');
    if(steps%2 == 0):
        start_colour, start_type, start_value, start_count = board[i,j].split('.');
        character=chr(i+65) 
        turn_start =  character +'.'+str(j+1)
        event.widget.config(bg='red')
    elif(steps%2 == 1):
        character=chr(i+65)
        turn_end = character +'.'+str(j+1)
        event.widget.config(bg='red')
        #ask for appropiate range
        turn_start_x_str, turn_start_y = turn_start.split('.')
        turn_start_x = ord(turn_start_x_str)-65
        thisrange = abs(int(turn_start_x)-i)+abs(int(turn_start_y)-1-j)
        turn_range = str(thisrange)
        thisturn = turn_start + ':' + turn_range + ':' + turn_end
        if(conquer(board,thisturn, thiscolour,1) == 1):
            thiscolour = switch_colour(thiscolour)
        repaint_gui(board);
    thisturn = turn_start + ':' + turn_range + ':' + turn_end
    steps = steps+1
    return;    

def guiboard(board):
    global list_bg_label
    global list_label
    global root
    global steps
    for i,row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour,piece_type,piece_value_str,piece_count = board[i,j].split('.')
            if(piece_type == 'P' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackpyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackcirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacktriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacksquare.gif').read())
            elif(piece_type == 'P' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitepyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitecirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitetriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitesquare.gif').read())
            else:
                bg_image=tk.PhotoImage(data=open('img/white.gif').read())
            bg_label = tk.Label(root, image=bg_image)
            bg_label.image=bg_image
            bg_label.place(x=i,y=j,relwidth=1, relheight=1)
            bg_label.grid(row=i,column=j,padx='1',pady='1')
            list_bg_label.append(bg_label)
            if(piece_value_str != '0'):
                L = tk.Label(root,text=piece_value_str,anchor='s')
            else:
                L = tk.Label(root,text='',anchor='s')
            
            L.grid(row=i,column=j,padx='1',pady='1')
            list_label.append(L)
            bg_label.bind('<Button-1>',lambda e, i=i,j=j,k=board: on_click(i,j,e,k))
            borderpiece_lower = tk.Label(root, text=str(j+1))
            borderpiece_lower.grid(row=i+1,column=j,padx='1',pady='1')
        borderpiece = tk.Label(root, text=chr(i+65))
        borderpiece.grid(row=i,column=j+1,padx='1',pady='1')        
    #when the board is drawn display possible turns in terminal
    if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                if(thiscolour == 'E'):
                    turns = getallturns(board, 'E') 
                    print(turns);
                    print(len(turns));
                elif(thiscolour == 'U'):
                    turns = getallturns(board, 'U') 
                    print(turns);
                    print(len(turns));
    return;

def printboard(board):
    print("|  A  |  B  |  C  |  D  |  E  |  F  |  G  |  H  |");
    print("-------------------------------------------------$");
    for i in range(16): #gehe Zeilenweise durch
        for h in range(3):
            print("|", end='')
            for j in range(8):  #baue zeile von rechts nach links auf
                blackwhite,piece_type,piece_value_str, piece_count = board[j, i].split('.');
                piece_value = int(piece_value_str);
                if (h == 0): 
                    if (blackwhite == 'U' and piece_type != 'P'):#erste Zeile
                        print("_____|", end='');
                    elif (blackwhite == 'U' and piece_type == 'P'):
                        print("__+__|", end='');
                    elif (blackwhite == 'E' and piece_type == 'P'):
                        print("  +  |", end='');
                    else: 
                        print("     |", end='')
                elif (h == 1):
                    if (piece_type == '2'):#round, partiens
                        print("(%03d)|" % piece_value, end='');
                    elif (piece_type == '3'): # superpartiens
                        print("<%03d>|" % piece_value, end='');
                    elif (piece_type == '4' or piece_type == 'P'): #superparticulares and pyramid
                        print("[%03d]|" % piece_value, end='');
                    else:
                        print("     |", end='')
                elif (h == 2):    # end of all lines
                    print("     |", end='')
            print(i+1);
        print("-------------------------------------------------$");
    print("|  A  |  B  |  C  |  D  |  E  |  F  |  G  |  H  |");
    return;

def printboard_landscape(board):
    print("|  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  | 10  | 11  | 12  | 13  | 14  | 15  | 16  |");
    print("-------------------------------------------------------------------------------------------------$");
    for i in range(8): #gehe Zeilenweise durch
        for h in range(3):
            print("|", end='')
            for j in range(16):  #baue zeile von rechts nach links auf
                blackwhite,piece_type,piece_value_str, piece_count = board[i, j].split('.');
                piece_value = int(piece_value_str);
                if (h == 0): 
                    if (blackwhite == 'U' and piece_type != 'P'):#erste Zeile
                        print("_____|", end='');
                    elif (blackwhite == 'U' and piece_type == 'P'):
                        print("__+__|", end='');
                    elif (blackwhite == 'E' and piece_type == 'P'):
                        print("  +  |", end='');
                    else: 
                        print("     |", end='')
                elif (h == 1):
                    if (piece_type == '2'):#round, partiens
                        print("(%03d)|" % piece_value, end='');
                    elif (piece_type == '3'): # superpartiens
                        print("<%03d>|" % piece_value, end='');
                    elif (piece_type == '4' or piece_type == 'P'): #superparticulares and pyramid
                        print("[%03d]|" % piece_value, end='');
                    else:
                        print("     |", end='')
                elif (h == 2):    # end of all lines
                    print("     |", end='')
            t = chr(i+65)        
            print("%s" % t); #print chars instead of numbers -> from A to H
        print("-------------------------------------------------------------------------------------------------$");
    print("|  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  | 10  | 11  | 12  | 13  | 14  | 15  | 16  |");
    return;

def path_free(start_x, start_y, length, end_x, end_y,board):
    possible_coords = [(start_x,start_y)]
    possible_coords_to_add = []
    visited_coords = []
    l_max = length
    l = length
    attacker = board[start_x,start_y];
    attacker_colour, attacker_type, attacker_value_str, attacker_count = attacker.split('.')
    while(l>0):# or (end_x,end_y) in possible_coords):
        for x in possible_coords:
            if(x not in visited_coords):
                visited_coords.append(x);
            if(x[0]<7):
                x_neu = x[0]+1
                if((board[x_neu,x[1]] == '0.0.0.0' and ((x_neu,x[1]) not in visited_coords) and ((x_neu,x[1]) not in possible_coords) and ((x_neu,x[1]) not in possible_coords_to_add) and ((abs(start_x-x_neu)+ abs(start_y-x[1]))<= l_max)) or ((x_neu,x[1]) == (end_x,end_y))):
                    possible_coords_to_add.append((x_neu,x[1]))
            if(x[0]>0):
                x_neu = x[0]-1
                if((board[x_neu,x[1]] == '0.0.0.0' and ((x_neu,x[1]) not in visited_coords) and ((x_neu,x[1]) not in possible_coords) and ((x_neu,x[1]) not in possible_coords_to_add) and ((abs(start_x-x_neu)+ abs(start_y-x[1]))<= l_max)) or ((x_neu,x[1]) == (end_x,end_y))):
                    possible_coords_to_add.append((x_neu,x[1]))
            if(x[1]>0):
                y_neu = x[1]-1
                if((board[x[0],y_neu] == '0.0.0.0' and ((x[0],y_neu) not in visited_coords) and ((x[0],y_neu) not in possible_coords) and ((x[0],y_neu) not in possible_coords_to_add) and ((abs(start_x-x[0])+ abs(start_y-y_neu))<= l_max)) or ((x[0],y_neu) == (end_x,end_y))):
                    possible_coords_to_add.append((x[0],y_neu))
            if(x[1]<15):
                y_neu = x[1]+1
                if((board[x[0],y_neu] == '0.0.0.0' and ((x[0],y_neu) not in visited_coords) and ((x[0],y_neu) not in possible_coords) and ((x[0],y_neu) not in possible_coords_to_add) and ((abs(start_x-x[0])+ abs(start_y-y_neu))<= l_max)) or ((x[0],y_neu) == (end_x,end_y))):
                    possible_coords_to_add.append((x[0],y_neu))
        l = l-1
        possible_coords = possible_coords_to_add
        possible_coords_to_add = []
    #if length has reached the end, check if we could reach the destination
    raid_possible = True
    #check whether turn is a raid:
    if((attacker_type == 'P' and length not in [2,3,4]) or (attacker_type == '2' and length != 2) or (attacker_type == '3' and length != 3) or (attacker_type == '4' and length != 4)):
        #check direction of the raid
        if(start_x - end_x > 0):
            for d in range(end_x+1,start_x-1):
                if(board[d,end_y] != '0.0.0.0'):
                    raid_possible = False
        elif(start_x - end_y <0): 
            for d in range(start_x+1,end_x-1):
                if(board[d,end_y] != '0.0.0.0'):
                    raid_possible = False
        elif(start_y - end_y >0):
            for d in range(end_y+1,start_y-1):
                if(board[end_x,d] != '0.0.0.0'):
                    raid_possible = False
        elif(start_y - end_y <0):    
            for d in range(start_y+1,end_y-1):
                if(board[end_x,d] != '0.0.0.0'):
                    raid_possible = False
    if((end_x, end_y) in (visited_coords+possible_coords) and raid_possible):
        return True;
    else:
        return False;
    
def remove(board,piece):
    global count_even;
    global count_uneven;
    count_even = 0;
    count_uneven = 0;
    x,y = numpy.where(board == piece);
    board[x,y] = '0.0.0.0';
    piece_colour, piece_type, piece_value_str, piece_count = piece.split('.');
    piece_value = int(piece_value_str);
    if(piece_colour == 'E'):
        count_even = count_even + piece_value;
    if(piece_colour == 'U'):
        count_uneven = count_uneven + piece_value;
    return;

def move(board,start_x, start_y, end_x, end_y):
    board[end_x, end_y] = board[start_x, start_y];
    board[start_x, start_y] = '0.0.0.0';
    return; 


def erase_pyramid(board,colour):
    if(colour == 'U'):
        for piece in ['U.3.64.0','U.4.49.0','U.2.49.0','U.3.16.0','U.3.36.0','U.2.25.0','U.3.25.0', 'U.P.190.0']:
            remove(board,piece);
    elif(colour == 'E'):
        for piece in ['E.2.4.0','E.2.4.1','E.3.9.0','E.2.16.0','E.4.25.0','E.3.25.0','E.2.36.0', 'E.P.91.0']:
            remove(board,piece);
    return;

def check_besieged(board, piece):
    if(piece == '0.0.0.0'):
        return False;
    colour, piece_type, piece_value_str, piece_count = piece.split('.');
    x,y = numpy.where(board == piece);
    if(x<7):
        north=str(board[int(x+1),int(y)]);
        north_colour,north_type,north_value, north_count = north.split('.');
    else:
        north_colour = colour;
    if(x>0):
        south_colour, south_type, south_value, west_count = str(board[int(x-1),int(y)]).split('.');
    else:
        south_colour = colour;
    if(y<15):
        west_colour, west_type, west_value, west_count = str(board[int(x),int(y+1)]).split('.');
    else:
        west_colour = colour;
    if(y>0):
        east_colour, east_type, east_value, east_count = str(board[int(x),int(y-1)]).split('.');
    else:
        east_colour = colour;
    colour_list = [north_colour, south_colour, east_colour, west_colour];
    colour_match = 0;
    #check, if colours match
    if (colour == 'E'):
        #check, if all colours surrounding are the opposite
        if(all( x == 'U' for x in colour_list)):
            colour_match = 1;
    elif (colour == 'U'):
        #check, if all colours surrounding are the opposite
        if(all( x == 'E' for x in colour_list)):
            colour_match = 1;
    if (colour_match == 1):
        return True;
    else:
        return False;
    return -1;

#check, if any combination of pieces and turn ranges matches value. Returns 1 if match, 2 if pyramid base match even, 3 if pyramid base match uneven, 0 else
def check_ops(board,attacker,piece, turn):
    if(piece == '0.0.0.0' or attacker == '0.0.0.0'):
        return 0;
    colour, piece_type, piece_value_str, piece_count = piece.split('.');
    piece_value = int(piece_value_str);
    #parse turn:
    start_field,turn_range_str,end_field = turn.split(':');
    turn_range = int(turn_range_str);
    #parse fields
    attacker_colour, attacker_type, attacker_value_str, attacker_count = attacker.split('.');
    attacker_value = int(attacker_value_str);
    if (attacker_colour != colour):
       # print("colours match")
        x,y = numpy.where(board == piece)
        if(x<7):
            north=str(board[int(x+1),int(y)]);
            north_colour,north_type,north_value, north_count = north.split('.');
            if(north == attacker):
                north_value = 0;
                north_colour = 0;
        else:
            north_value = 0;
            north_colour = '0';
        if(x>0):
            south = str(board[int(x-1),int(y)])
            south_colour, south_type, south_value, south_count = south.split('.');
            if(south == attacker):
                south_value = 0;
                south_colour = 0;
        else:
            south_value = 0;
            south_colour = '0';
        if(y<15):
            west = str(board[int(x),int(y+1)])
            west_colour, west_type, west_value, west_count = west.split('.');
            if(west == attacker):
                west_value = 0;
                west_colour = 0;
        else:
            west_value = 0;
            west_colour = '0';
        if(y>0):
            east = str(board[int(x),int(y-1)])
            east_colour, east_type, east_value, east_count = east.split('.');
            if(east == attacker):
                east_value = 0;
                east_colour = 0;
        else:
            east_value = 0;
            east_colour = '0';
        value_list = [int(north_value), int(south_value), int(east_value), int(west_value)];
        colour_list = [north_colour, south_colour, east_colour, west_colour];
        #get indices of pieces with the same colour
        indices = [i for i,x in enumerate(colour_list) if x == attacker_colour]
        #attacker_value {+-*/} turn_range {+-*/} north_value, south_value, east_value, west_value == piece_value
        #build one large list of values:
        total_list = []
        total_list.append(float(attacker_value));
        total_list.append(float(attacker_value + turn_range));
        total_list.append(float(attacker_value - turn_range));
        total_list.append(float(attacker_value * turn_range));
        if(float(turn_range) == float(0)):
            turn_range = 1
        total_list.append(float(float(attacker_value) / float(turn_range)));
        latest_added = [2, 3, 4, 5];
        current_added = [];
        for i in indices:
            for j in latest_added:
                total_list.append(float(total_list[j-1] + float(value_list[i])))
                current_added.append(j * 4 -2)
                total_list.append(float(total_list[j-1] - value_list[i]))
                current_added.append(j * 4 -1)
                total_list.append(float(total_list[j-1] * value_list[i]))
                current_added.append(j * 4)
                if(value_list[i]!=0):
                    total_list.append(float(float(total_list[j-1]) / float(value_list[i])))
                else:
                    total_list.append(float(0))
                current_added.append(j * 4 +1)
            latest_added = current_added;
            current_added = []
        #if raid, only check if values match, if ambuscade also check, if there are adjacent pieces of same color
        if(turn!='0.0:0:0.0' or (turn == '0.0:0:0.0' and attacker_colour in colour_list)):
            #check, if values match
            if(float(piece_value) in total_list[1:]):
                if(why):
                    print(indices)
                    print(colour_list)
                    print(value_list)
                    print(total_list)
                return 1;
        elif(float(36) in total_list[1:] and piece_type == 'P' and colour == 'E'):
            return 2;
        elif(float(64) in total_list[1:] and piece_type == 'P' and colour == 'U'):
            return 3;
    return 0;

#if real is set, don't move or remove pieces

def conquer(board, turn, playing_colour, real):
    #parse turn:
    start_field,turn_range_str,end_field = turn.split(':');
    turn_range = int(turn_range_str);
    turn_range_str = str(turn_range); #now we should have gotten rid of any padding
    #parse fields
    attacker_x_str, attacker_y_str = start_field.split('.');
    defender_x_str, defender_y_str = end_field.split('.');
    attacker_x = ord(attacker_x_str)-65; #change Char to int
    defender_x = ord(defender_x_str)-65;
    attacker_y = int(attacker_y_str)-1
    defender_y = int(defender_y_str)-1
    attacker = board[attacker_x,attacker_y];
    defender = board[defender_x,defender_y];
    attacker_colour, attacker_type, attacker_value_str, attacker_count = attacker.split('.');
    defender_colour, defender_type, defender_value_str, defender_count = defender.split('.');
    attacker_value = int(attacker_value_str)
    defender_value = int(defender_value_str)
    free_path = path_free(attacker_x, attacker_y,turn_range,defender_x,defender_y, board); 
    regular_turn = False;
    moved = False;
    wrongside = False;
    raid_possible = False;
    #check if the wrong side is playing:
    if(attacker_colour != playing_colour):
        wrongside = True;
    #check, if a raid is possible:
    if((attacker_x-defender_x == 0) or (attacker_y-defender_y == 0)):
        raid_possible = True;
    #check, if the turn was a regular one
    if ((((attacker_type == '2' or attacker_type == 'P') and turn_range_str == '2') or ((attacker_type == '3' or attacker_type == 'P') and turn_range_str == '3') or ((attacker_type == '4' or attacker_type == 'P') and turn_range_str == '4')) and (playing_colour == attacker_colour and playing_colour != defender_colour) and free_path ):
        regular_turn = True;
    #check, if Zugweite matches type, then compare values
    if(regular_turn):
        if((((attacker_colour == 'E' and defender_colour == 'U') or (attacker_colour == 'U' and defender_colour == 'E')) and attacker_value == defender_value)): #normal turn meeting
            if real: 
                remove(board, defender);
                move(board,attacker_x, attacker_y, defender_x, defender_y);
                moved = True;
                if(why): print('Meeting killed %s' % defender)
        elif(defender_type == 'P' and defender_colour == 'E' and attacker_value == 36): #erased pyramid uneven
            if real: 
                erase_pyramid(board,'E');
                move(board,attacker_x, attacker_y, defender_x, defender_y);
                moved = True;
                if(why): print('Meeting killed %s' % defender)
        elif(defender_type == 'P' and defender_colour == 'U' and attacker_value == 64): #erased pyramid even
            if real: 
                erase_pyramid(board,'U');
                move(board,attacker_x, attacker_y, defender_x, defender_y);
                moved = True;
                if(why): print('Meeting killed %s' % defender)
        if(not moved and defender == '0.0.0.0'):  #moved on an empty field
            if real: move(board,attacker_x, attacker_y, defender_x, defender_y);
            moved = True;
    if(attacker_colour == playing_colour and free_path and raid_possible):
        #check for operation with and without adjacent pieces  
        ops_def = check_ops(board, attacker, defender, turn)
        if(ops_def == 1):
            if real:
                remove(board,defender);
                if(why): print('Raid killed %s' % defender)
            if(not moved): 
                if real: move(board,attacker_x, attacker_y, defender_x, defender_y);
                moved = True;
        elif(ops_def == 2):
            if real: 
                erase_pyramid(board,'E');
                if(why): print('Raid killed %s' % defender)
            if(not moved): 
                if real: move(board,attacker_x, attacker_y, defender_x, defender_y);
                moved = True;
        elif(ops_def == 3):
            if real: 
                erase_pyramid(board,'U');
                if(why): print('Meeting killed %s' % defender)
            if(not moved): 
                if real: move(board,attacker_x, attacker_y, defender_x, defender_y);
                moved = True;    
    #check for ambuscade: check whether operation and corresponding values may match (only possible after a regular turn or having moved there by raid) (check any piece adjacent to end position)
    if(moved):    
        if(defender_y<15):
            north = board[defender_x, defender_y+1];
            ops_north = check_ops(board, attacker, north, '0.0:0:0.0')
            if(ops_north == 1):
                if (real): 
                    remove(board,north);
                    if(why): print('Ambush killed %s' % north)
            elif(ops_north == 2):
                if (real): 
                    erase_pyramid(board,'E');
                    if(why): print('Ambush killed %s' % north)
            elif(ops_north == 3):
                if (real): 
                    erase_pyramid(board,'U');
                    if(why): print('Ambush killed %s' % north)
        if(defender_x<7):
            east = board[defender_x+1, defender_y];
            ops_east = check_ops(board, attacker,east, '0.0:0:0.0')
            if(ops_east == 1):
                if (real): 
                    remove(board,east);
                    if(why): print('Ambush killed %s' % east)
            elif(ops_east == 2):
                if (real): 
                    erase_pyramid(board,'E');
                    if(why): print('Ambush killed %s' % east)
            elif(ops_east == 3):
                if (real): 
                    erase_pyramid(board,'U');
                    if(why): print('Ambush killed %s' % east)
        if(defender_y>0):
            south = board[defender_x, defender_y-1];
            ops_south = check_ops(board, attacker,south, '0.0:0:0.0')
            if(ops_south == 1):
                if (real): 
                    remove(board,south);
                    if(why): print('Ambush killed %s' % south)
            elif(ops_south == 2):
                if (real): 
                    erase_pyramid(board,'E');
                    if(why): print('Ambush killed %s' % south)
            elif(ops_south == 3):
                if (real): 
                    erase_pyramid(board,'U');
                    if(why): print('Ambush killed %s' % south)
        if(defender_x>0):
            west = board[defender_x-1, defender_y];
            ops_west = check_ops(board, attacker,west, '0.0:0:0.0')
            if(ops_west in [1,2,3] and why):
                    print('Ambush killed %s' % west)
            if(ops_west == 1):
                if (real): remove(board,west);
            elif(ops_west == 2):
                if (real): erase_pyramid(board,'E');
            elif(ops_west == 3):
                if (real): erase_pyramid(board,'U');
    #check for neighbouring pieces, and if these are besieged (occurs after movement only)    
    if(moved):
      #  if real: printboard_landscape(board);
        if(defender_y<15):
            north = board[defender_x, defender_y+1];
            if(check_besieged(board, north)):
                if real: remove(board,north);
                if(why):
                    print('Siege killed %s' % north)
        if(defender_x<7):
            east = board[defender_x+1, defender_y];
            if(check_besieged(board, east)):
                if real: remove(board,east);
                if(why): print('Siege killed %s' % east)
        if(defender_y>0):
            south = board[defender_x, defender_y-1];
            if(check_besieged(board, south)):
                if real: remove(board,south);
                if(why): print('Siege killed %s' % south)
        if(defender_x>0):
            west = board[defender_x-1, defender_y];
            if(check_besieged(board, west)):
                if real: remove(board,west);
                if(why): print('Siege killed %s' % west)
        if('-log' in sys.argv and real == 1): #logging enabled
                logindex = sys.argv.index('-log')
                filestring = sys.argv[logindex+1]
                with open(filestring, "a") as logfile:
                    logfile.write("Playing Color: %s Turn: %s \n" % (playing_colour,turn))        
        return 1;        
    elif(wrongside):
        if real: print("You used the wrong side!");
        if(gui):
            if real: tkMessageBox.showerror("Error", "You used the wrong side, please repeat")
    else:        
        if real: 
            print("turn not allowed, please repeat!");
            print("wrongside: %s" % wrongside)
            print("regular_turn: %s" % regular_turn)
            print("raid_possible: %s" % raid_possible)
        if(gui):
            if real: tkMessageBox.showerror("Error", "This turn is not allowed, please repeat")
    return 0;


#now starting ki-development: first, get all possible turns:
def is_movable(x,y,board):
    #first check on y direction, as this is killing expected runtime
    movable = False;
    if(y<15):
        north=str(board[int(x),int(y+1)]);
        north_colour,north_type,north_value, north_count = north.split('.');
        if(north_colour == '0'):
            return True;
    else:
        north_colour = 'B'; #introduce a blocking color B
    if(y>0):
        south_colour, south_type, south_value, west_count = str(board[int(x),int(y-1)]).split('.');
        if(south_colour == '0'):
            return True;
    else:
        south_colour = 'B';
    if(x<7):
        west_colour, west_type, west_value, west_count = str(board[int(x+1),int(y)]).split('.');
        if(west_colour == '0'):
            return True;
    else:
        west_colour = 'B';
    if(x>0):
        east_colour, east_type, east_value, east_count = str(board[int(x-1),int(y)]).split('.');
        if(east_colour == '0'):
            return True;
    else:
        east_colour = 'B';
    return False;

def getallturns(board, colour):
    list_of_turns = []
    pieces_on_board = []
    for i, row in enumerate(board):
        for j,column in enumerate(row):
            if(board[i,j] != '0.0.0.0'):
                pieces_on_board.append(board[i,j])
    output = mp.Queue()
    processes = [mp.Process(target=turns_per_piece, args=(piece,colour,board, output)) for piece in pieces_on_board]
    for p in processes:
        p.start()
    for p in processes:
        p.join()
    results = [output.get() for p in processes]
    for r in results:
        list_of_turns.extend(r)
    return list_of_turns;

def turns_per_piece (piece, colour, board_input, output):
    board = numpy.copy(board_input)
    list_of_turns = []
    piece_colour, piece_type, piece_value_str, piece_count = piece.split('.')
    if(piece_colour == colour):
        x,y = numpy.where(board == piece);
        #check, whether piece can move:
        if(is_movable(x,y,board)):
            character = chr(x+65)
            turn_start = character+'.'+ str(int(y+1))
            for i in range(8):
                for j in range(16):
                    turn_range = int(abs(i-x) + abs(j-y))
                    character_end = chr(i+65)
                    turn_end=character_end + '.' + str(j+1)
                    test_turn = turn_start + ':' + str(turn_range) + ':' + turn_end
                    if((piece_type == '3' and turn_range == 3) or (piece_type == '2' and turn_range == 2) or (piece_type == '4' and turn_range == 4) or (piece_type == 'P' and turn_range in [2,3,4])): 
                        raid_possible = True
                    if((piece_type == '3' and turn_range != 3) or (piece_type == '2' and turn_range != 2) or (piece_type == '4' and turn_range != 4) or (piece_type == 'P' and turn_range not in [2,3,4])):
                        #check if paths are made in straight lines during raids
                        raid_possible = True if(i==x or j == y) else False
                    if(conquer(board,test_turn,piece_colour,0) != 0 and is_movable(i,j,board) and raid_possible): # only move there if the target piece is movable itself (aka has a reachable point)
                        list_of_turns.append(test_turn);
    output.put(list_of_turns);

def get_points(board, colour):
    total_value = 0
    for i, row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour, piece_type, piece_value_str, piece_count = str(board[i,j]).split('.');
            if(piece_colour == colour):
                total_value = total_value + int(piece_value_str);
    return total_value;

    
def play(board):
    global gui
    global root
    end = 1
    even_turn = 1
    playing_colour = 'E'
    if('--gui' in sys.argv or '-gui' in sys.argv):
        gui = 1
        root = tk.Tk()
        guiboard(board)
        tkMessageBox.showinfo("Information", " Willkommen zu einer Partie Rhithomachie. Weiss beginnt. Einfach auf die entsprechenden Felder klicken.")
        root.mainloop()
    else:
        while(end == 1):
            if('-l' in sys.argv or '--landscape' in sys.argv):
                printboard_landscape(board)
            else:
                printboard(board)
            if(even_turn == 1):
                print("EVEN is playing. State your turn (<start_position>:<path length>:<end_position>. To end the game, simply type 'finish' or 'q'");
                even_turn = 0;
                playing_colour = 'E';
            elif(even_turn == 0):
                print("UNEVEN is playing. State your turn (<start_position>:<path length>:<end_position>. To end the game, simply type 'finish' or 'q'");
                even_turn = 1;
                playing_colour = 'U';
            if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                board_copy = numpy.copy(board)
                turns = getallturns(board_copy, playing_colour) 
                print(turns);
                print(len(turns));
            turn = str(raw_input("Your turn:"));
            if(turn == 'finish' or turn == 'q'):
                break;
            if(conquer(board,turn, playing_colour,1) == 0): #in case of wrong turn: stay by colour
                even_turn = abs(even_turn-1)
    return;



def board_after(board,turns):
    for t in turns:
        #parse turn:
        start_field,turn_range_str,end_field = t.split(':');
        #parse fields
        attacker_x_str, attacker_y_str = start_field.split('.');
        attacker_x = ord(attacker_x_str)-65; 
        attacker_y = int(attacker_y_str)-1
        attacker = board[attacker_x,attacker_y];
        attacker_colour, attacker_type, attacker_value_str, attacker_count = attacker.split('.');
        conquer(board,t,attacker_colour,1)
    return board;    

def switch_colour(colour):
    if(colour == 'E'):
        colour = 'U'
    else:
        colour = 'E'
    return colour;

def recvall(socket):
    data = ''
    while len(data) < 13:
        packet = socket.recv(1024)
        if not packet: break
        data += packet
    return data

#client thread: sends a turn and awaits reply
def client_thread(board,adress, port, protocol):
    global root
    legal_turn = 0;
    if(protocol == 0): #ipv4
            s = socket.socket()
    if(protocol == 1): #ipv6
            s = socket.socket(family=socket.AF_INET6)
    if(gui):
        s = socket.create_connection((adress,int(port)))
        root = tk.Tk()
        guiboard_mp(board,s,'E',0)
        root.mainloop()
        s.close()
    else:
        while 1:
            s = socket.create_connection((adress,int(port)))
            try:
                turn = recvall(s)
                conquer(board,turn,'E',1)
                while(legal_turn != 1):
                    if('-l' in sys.argv or '--landscape' in sys.argv):
                        printboard_landscape(board)
                    else:
                        printboard(board)
                    if('-ai' in sys.argv or '--ai' in sys.argv):
                        print("list of possible turns:");
                        board_copy = numpy.copy(board)
                        turns = getallturns(board_copy, 'U') 
                        print(turns);
                        print(len(turns));    
                    print("State your turn (Uneven):")
                    turn_new = str(raw_input("Your turn:"));
                    legal_turn = conquer(board,turn_new,'U',1)
                #padding of turn_new before sending:
                new_begin, new_range, new_end = turn_new.split(':')
                missing = 13-len(turn_new);
                for i in range(missing):
                    new_range = '0' + new_range
                turn_to_send = new_begin + ':' + new_range + ':' + new_end;
                s.sendall(turn_to_send)
            finally:
                s.close()    
    return;
    
#server thread: gets a turn delivered, then processes it
def server_thread(board,adress,port,protocol):
    global root
    global serverSocket
    root = tk.Tk()
    last_turn = '0.0:0:0.0'
    legal_turn = 0
    #current_turn = '0.0:0:0.0'
    if(protocol == 0):
        sendSocket = socket.socket()
    if(protocol == 1):
        sendSocket = socket.socket(family=socket.AF_INET6)
    sendSocket.bind((adress,int(port)))
    sendSocket.listen(5)
    while 1:    
        c,adr = sendSocket.accept()
        if(gui):
            guiboard_mp(board,c,'U',1)
            root.mainloop()
            c.close()
            sendSocket.close()
            break;
        else:    
            while 1:
                legal_turn = 0
                while(legal_turn != 1):
                    if('-l' in sys.argv or '--landscape' in sys.argv):
                        printboard_landscape(board)
                    else:
                        printboard(board)
                    if('-ai' in sys.argv or '--ai' in sys.argv):
                        print("list of possible turns:");
                        board_copy = numpy.copy(board)
                        turns = getallturns(board_copy, 'E') 
                        print(turns);
                        print(len(turns));    
                    print("State your turn (Even):")
                    turn_new = str(raw_input("Your turn:"));
                    legal_turn = conquer(board,turn_new,'E',1)
                    #padding of turn_new before sending:
                new_begin, new_range, new_end = turn_new.split(':')
                missing = 13-len(turn_new);
                for i in range(missing):
                    new_range = '0' + new_range
                turn_to_send = new_begin + ':' + new_range + ':' + new_end;
                try:
                    c.sendall(turn_to_send)
                    last_turn = recvall(c)
                    conquer(board,last_turn,'U',1)
                finally:    
                    c.close()
    return;

def play_mp(board,colour,adress,port,protocol):
    global gui
    current_colour = 'E'
    if('-gui' in sys.argv or '--gui' in sys.argv):
        gui = 1
    if(colour == 'U'): #convention: Player even hosts the server
        server_thread(board,adress,port,protocol)
    if(colour == 'E'):
        client_thread(board,adress,port,protocol)        
    return;

def on_click_mp(i,j,event,board,s,colour,proper):
    global thisturn
    global steps
    global thisrange
    global root
    global first_turn
    global thiscolour
    global serverSocket
    thiscolour = colour
    turn_start, turn_range, turn_end = thisturn.split(':');
    if(steps%2 == 0):
        start_colour, start_type, start_value, start_count = board[i,j].split('.');
        character=chr(i+65) 
        turn_start =  character +'.'+str(j+1)
        event.widget.config(bg='red')
        proper = 0
    elif(steps%2 == 1):
        character=chr(i+65)
        turn_end = character +'.'+str(j+1)
        event.widget.config(bg='red')
        #ask for appropiate range
        turn_start_x_str, turn_start_y = turn_start.split('.')
        turn_start_x = ord(turn_start_x_str)-65
        thisrange = abs(int(turn_start_x)-i)+abs(int(turn_start_y)-1-j)
        turn_range = str(thisrange)
        thisturn = turn_start + ':' + turn_range + ':' + turn_end
        proper = conquer(board,thisturn, thiscolour,1) 
        if(proper == 1):
            #padding of turn_new before sending:
            new_begin, new_range, new_end = thisturn.split(':')
            missing = 13-len(thisturn);
            for i in range(missing):
                new_range = '0' + new_range
            turn_to_send = new_begin + ':' + new_range + ':' + new_end;
            print("sending:")
            print(turn_to_send)          
            s.sendall(turn_to_send)
            #repaint
            thiscolour = switch_colour(thiscolour)
        print("end of turn")
        repaint_gui_mp(board,s,colour,proper) # repaint twice, now with waiting for remote turns
    thisturn = turn_start + ':' + turn_range + ':' + turn_end
    steps = steps+1
    return;    

def repaint_gui_mp(board,s,colour,proper):
    global list_bg_label
    global list_label
    u = 0
    v = 0
    if(colour == 'E' and proper == 1): #Client side, first draw then send
        print("awaiting turn:")
        tkMessageBox.showinfo("Warte", "Erwarte Zug des Gegners (Schwarz)\n Das Spiel ist solange eingefroren.")
        next_turn = recvall(s)
        if(next_turn == ''):
            print('this is the end');
            tkMessageBox.showinfo("Ende", "Der Gegner hat das Spiel beendet.")
            sys.exit();
        print(next_turn)
        conquer(board,next_turn,'U',1)
        print("next turn executed")
        thiscolour = 'U'
    elif(colour == 'U' and proper == 1):#server side: open up new socket
        print("awaiting turn:")
        tkMessageBox.showinfo("Warte", "Erwarte Zug des Gegners (Weiss)\n Das Spiel ist solange eingefroren.")
        next_turn = recvall(s)
        if(next_turn == ''):
            print('this is the end');
            tkMessageBox.showinfo("Ende", "Der Gegner hat das Spiel beendet.")
            sys.exit();
        print(next_turn)    
        conquer(board,next_turn,'E',1)
        print("next turn executed")
        thiscolour = 'E'
    for i,row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour,piece_type,piece_value_str,piece_count = board[i,j].split('.')
            if(piece_type == 'P' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackpyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackcirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacktriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacksquare.gif').read())
            elif(piece_type == 'P' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitepyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitecirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitetriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitesquare.gif').read())
            else:
                bg_image=tk.PhotoImage(data=open('img/white.gif').read())
            bg_label = list_bg_label[u]
            L = list_label[v]
            u = u+1
            v = v+1
            bg_label.configure(image=bg_image, bg='white')
            bg_label.image=bg_image
            if(piece_value_str != '0'):
                L.configure(text=piece_value_str,anchor='s')
            else:
                L.configure(text='',anchor='s')
    #when the board is drawn display possible turns in terminal
    if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                if(thiscolour == 'E'):
                    turns = getallturns(board, 'U') 
                    print(turns);
                    print(len(turns));
                elif(thiscolour == 'U'):
                    turns = getallturns(board, 'E') 
                    print(turns);
                    print(len(turns));
    return;

def guiboard_mp(board,s,colour,proper):
    global root
    global steps
    global first_turn
    global serverSocket
    global thiscolour
    global list_label
    global list_bg_label
    if(colour == 'E' and proper == 1): #Client side, first draw then send
        print("awaiting turn:")
        tkMessageBox.showinfo("Warte", "Erwarte Zug des Gegners (Schwarz)\n Das Spiel ist solange eingefroren.")
        next_turn = recvall(s)
        if(next_turn == ''):
            print('this is the end');
            tkMessageBox.showinfo("Ende", "Der Gegner hat das Spiel beendet.")
            sys.exit();
        print(next_turn)
        conquer(board,next_turn,'U',1)
        print("next turn executed")
        thiscolour = 'U'
    elif(colour == 'U' and proper == 1):#server side: open up new socket
        print("awaiting turn:")
        tkMessageBox.showinfo("Warte", "Erwarte Zug des Gegners (Weiss)\n Das Spiel ist solange eingefroren.")
        next_turn = recvall(s)
        if(next_turn == ''):
            print('this is the end');
            tkMessageBox.showinfo("Ende", "Der Gegner hat das Spiel beendet.")
            sys.exit();
        print(next_turn)
        conquer(board,next_turn,'E',1)
        print("next turn executed")
        thiscolour = 'E'    
    #build board    
    for i,row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour,piece_type,piece_value_str,piece_count = board[i,j].split('.')
            if(piece_type == 'P' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackpyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackcirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacktriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacksquare.gif').read())
            elif(piece_type == 'P' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitepyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitecirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitetriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitesquare.gif').read())
            else:
                bg_image=tk.PhotoImage(data=open('img/white.gif').read())
            bg_label = tk.Label(root, image=bg_image)
            bg_label.image=bg_image
            bg_label.place(x=i,y=j,relwidth=1, relheight=1)
            bg_label.grid(row=i,column=j,padx='1',pady='1')
            list_bg_label.append(bg_label)
            if(piece_value_str != '0'):
                L = tk.Label(root,text=piece_value_str,anchor='s')
            else:
                L = tk.Label(root,text='',anchor='s')
            L.grid(row=i,column=j,padx='1',pady='1')
            list_label.append(L)
            bg_label.bind('<Button-1>',lambda e, i=i,j=j,k=board,l=s,m=colour,p=proper: on_click_mp(i,j,e,k,l,m,p))
            borderpiece_lower = tk.Label(root, text=str(j+1))
            borderpiece_lower.grid(row=i+1,column=j,padx='1',pady='1')
        borderpiece = tk.Label(root, text=chr(i+65))
        borderpiece.grid(row=i,column=j+1,padx='1',pady='1')
    #when the board is drawn display possible turns in terminal
    if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                if(thiscolour == 'E'):
                    turns = getallturns(board, 'U') 
                    print(turns);
                    print(len(turns));
                elif(thiscolour == 'U'):
                    turns = getallturns(board, 'E') 
                    print(turns);
                    print(len(turns));
    return;