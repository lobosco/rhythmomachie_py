#!/usr/bin/python

from __future__ import print_function #needed for print syntax
from operator import attrgetter #needed to sort RNodes
import numpy
import rhythmo_base as rb
import anytree
import copy
import time
import sys
import Tkinter as tk
import tkMessageBox
import random

board = numpy.full((8,16), '0.0.0.0', dtype=object) 
#initial pieces
#uneven
board[7, 0] = 'U.4.361.0'
board[7, 1] = 'U.P.190.0'
board[7, 2] = 'U.3.100.0'
board[6, 0] = 'U.4.225.0'
board[6, 1] = 'U.4.120.0'
board[6, 2] = 'U.3.90.0'
board[1, 0] = 'U.4.121.0'
board[1, 1] = 'U.4.66.0'
board[1, 2] = 'U.3.25.0'
board[0, 0] = 'U.4.49.0'
board[0, 1] = 'U.4.28.0'
board[0, 2] = 'U.3.16.0'
board[5, 1] = 'U.3.64.0'
board[5, 2] = 'U.2.81.0'
board[5, 3] = 'U.2.9.0'
board[4, 1] = 'U.3.56.0'
board[4, 2] = 'U.2.49.0'
board[4, 3] = 'U.2.7.0'
board[3, 1] = 'U.3.30.0'
board[3, 2] = 'U.2.25.0'
board[3, 3] = 'U.2.5.0'
board[2, 1] = 'U.3.36.0'
board[2, 2] = 'U.2.9.1'
board[2, 3] = 'U.2.3.0'
#even side
board[0, 15] = 'E.4.289.0'
board[0, 14] = 'E.4.153.0'
board[0, 13] = 'E.3.81.0'
board[1, 15] = 'E.4.169.0'
board[1, 14] = 'E.P.91.0'
board[1, 13] = 'E.3.72.0'
board[6, 15] = 'E.4.81.0'
board[6, 14] = 'E.4.45.0'
board[6, 13] = 'E.3.6.0'
board[7, 15] = 'E.4.25.0'
board[7, 14] = 'E.4.15.0'
board[7, 13] = 'E.3.9.0'
board[2, 14] = 'E.3.49.0'
board[2, 13] = 'E.2.64.0'
board[2, 12] = 'E.2.8.0'
board[3, 14] = 'E.3.42.0'
board[3, 13] = 'E.2.36.0'
board[3, 12] = 'E.2.6.0'
board[4, 14] = 'E.3.20.0'
board[4, 13] = 'E.2.16.0'
board[4, 12] = 'E.2.4.0'
board[5, 14] = 'E.3.25.0'
board[5, 13] = 'E.2.4.1'
board[5, 12] = 'E.2.2.0'


root = 0
steps = 0
first_turn = 0
thiscolour = 'E'
thisturn = '0.0:0:0.0'
list_label = []
list_bg_label = []

class RNode:
    def __init__(self,list_of_turns,board,colour):
        self.turns=list_of_turns;
        current_board = rb.board_after(board,list_of_turns)
        self.points_even = rb.get_points(current_board,'E')
        self.points_uneven = rb.get_points(current_board,'U')
        self.playing_colour = colour

def max_list(a,key=None):
    if key is None:
        key = lambda x: x
    m, max_list = key(a[0]), []
    for s in a:
        k = key(s)
        if k > m:
            m, max_list = k, [s]
        elif k == m:
            max_list.append(s)
    return max_list;

def min_list(a,key=None):
    if key is None:
        key = lambda x: x
    m, min_list = key(a[0]), []
    for s in a:
        k = key(s)
        if k < m:
            m, min_list = k, [s]
        elif k == m:
            min_list.append(s)
    return min_list;

def choose_bests(nodelist,colour,policy,sample):
    #list all Nodes, get an array of the best ones, return that list
    if(policy == "defensive" and colour=='E'):
        best_nodes = max_list(nodelist, key=attrgetter('points_even'))
    elif(policy == 'destructive' and colour=='E'):
        best_nodes = min_list(nodelist, key=attrgetter('points_uneven'))
    elif(policy == "defensive" and colour=='U'):
        best_nodes = max_list(nodelist, key=attrgetter('points_uneven'))
    elif(policy == 'destructive' and colour=='U'):
        best_nodes = min_list(nodelist, key=attrgetter('points_even'))
    n = 40
    if(len(best_nodes) > n and sample==1):
        sample_nodes = numpy.random.choice(best_nodes,size=n,replace=False,p=None)
    else:
        sample_nodes=best_nodes
    print("best nodes:", len(best_nodes))
    print("sample nodes:", len(sample_nodes))
    #for i in best_nodes:
        #print(i.turns)
    #print("sample nodes:")    
    #for j in sample_nodes:
        #print(j.turns)
    #print("all nodes done")
    return sample_nodes

def choose_best(nodelist,colour,policy):
    #list all Nodes, get an array of the best ones, either return arbitrarily one or all 
    if(policy == "defensive" and colour=='E'):
        best_node = max(nodelist, key=attrgetter('points_even'))
    elif(policy == 'destructive' and colour=='E'):
        best_node = min(nodelist, key=attrgetter('points_uneven'))
    elif(policy == "defensive" and colour=='U'):
        best_node = max(nodelist, key=attrgetter('points_uneven'))
    elif(policy == 'destructive' and colour=='U'):
        best_node = min(nodelist, key=attrgetter('points_even'))
    return best_node;

def get_next_turn_probabilistic(board,depth,colour,policy):
    #get all turns 
    start = time.time()
    current_nodes = []
    next_level_nodes = []
    colour_new = colour
    while(depth>0):
        if(len(current_nodes)<1):
            possible_turns = rb.getallturns(board,colour)
            for n in possible_turns:
                board_new = numpy.copy(board)
                turnlist = [n]
                next_node = RNode(turnlist,board_new,colour)
                current_nodes.append(next_node)
            current_nodes = choose_bests(current_nodes,colour_new,policy,0)        
        for x in current_nodes:
            board_new = numpy.copy(board)
            current_board = rb.board_after(board_new,x.turns)
            #rb.printboard_landscape(current_board)
            if(x.playing_colour == 'E'):
                colour_new = 'U'
            else:
                colour_new = 'E'                
            possible_turns = rb.getallturns(current_board,colour_new);
            #print(possible_turns)
            for n in possible_turns:
                board_newest = numpy.copy(board)
                turnlist_new = copy.deepcopy(x.turns)
                turnlist_new.append(n)
                #print(n)
                #print(turnlist_new)
                next_node = RNode(turnlist_new,board_newest,colour_new)
                next_level_nodes.append(next_node)
        depth = depth-1
        print("depth: %s" %depth)
        print("next nodes:", len(next_level_nodes))
        current_nodes=choose_bests(next_level_nodes,colour_new,policy,1)
        next_level_nodes = []
        if(len(current_nodes) == 1): #if there is only one possibility to add, finish loop
            break;
        
    best_node = choose_best(current_nodes,colour_new,policy)
    end = time.time()
    print("elapsed time in s:")
    print(end-start) 
    return best_node.turns[0];

def get_next_turn(board,depth,colour,policy):
    #first, create a "tree-stump"
    #then go to last level and check for any result matching the policy
    #policies are:
        #defensive: highest own points
        #destructive: lowest oponent points
    start = time.time()
    current_nodes = []
    next_level_nodes = []
    
    while(depth>0):
        if(not current_nodes):
            possible_turns = rb.getallturns(board,colour)
            for n in possible_turns:
                board_new = numpy.copy(board)
                turnlist = [n]
                next_node = RNode(turnlist,board_new,colour)
                current_nodes.append(next_node)
        for x in current_nodes:
            board_new = numpy.copy(board)
            current_board = rb.board_after(board_new,x.turns)
            #rb.printboard_landscape(current_board)
            if(x.playing_colour == 'E'):
                colour_new = 'U'
            else:
                colour_new = 'E'                
            possible_turns = rb.getallturns(current_board,colour_new)
           # print(possible_turns)
            for n in possible_turns:
                board_newest = numpy.copy(board)
                turnlist_new = copy.deepcopy(x.turns)
                turnlist_new.append(n)
            #    print(n)
             #   print(turnlist_new)
                next_node = RNode(turnlist_new,board_newest,colour_new)
                next_level_nodes.append(next_node)
        depth = depth-1
        current_nodes = next_level_nodes
        next_level_nodes = []
        
    
    if(policy == "defensive" and colour=='E'):
        best_node = max(current_nodes, key=attrgetter('points_even'))
    elif(policy == 'destructive' and colour=='E'):
        best_node = min(current_nodes, key=attrgetter('points_uneven'))
    elif(policy == "defensive" and colour=='U'):
        best_node = max(current_nodes, key=attrgetter('points_uneven'))
    elif(policy == 'destructive' and colour=='U'):
        best_node = min(current_nodes, key=attrgetter('points_even'))
    
    optimal_next_turn = best_node.turns[0];
    end = time.time()
    print("elapsed time in s:")
    print(end-start)
    return optimal_next_turn;

def simulate_logfile(board,logfile):
    rb.why = True #turn statement on killing on
    list_all_turns = []
    with open(logfile) as f:
        for line_to_strip in f:
            line = line_to_strip.rstrip('\n')
            colour_str,turn = line.split('Turn: ')
            if(turn != ' '):
                print(turn)
                list_all_turns.append(turn)
        f.close()
    #print(list_all_turns)    
    for turn in list_all_turns:
        print(turn)
        board = rb.board_after(board,[turn])
        rb.printboard_landscape(board)
        step = str(raw_input("Next Turn? [n,q]:"));
        if(step != 'n'):
            return
    return

def start_after_logfile(board,logfile):
    rb.why = True #turn statement on killing on
    list_all_turns = []
    with open(logfile) as f:
        for line_to_strip in f:
            line = line_to_strip.rstrip('\n')
            colour_str,turn = line.split('Turn: ')
            if(turn != ' '):
                print(turn)
                list_all_turns.append(turn)
        f.close()
    #print(list_all_turns)    
    board = rb.board_after(board,list_all_turns)
    rb.play(board)
    return


def on_click_ai(i,j,event,board,colour,proper,policy):
    global thisturn
    global steps
    global thisrange
    global root
    global first_turn
    global thiscolour
    thiscolour = colour
    turn_start, turn_range, turn_end = thisturn.split(':');
    if(steps%2 == 0):
        start_colour, start_type, start_value, start_count = board[i,j].split('.');
        character=chr(i+65) 
        turn_start =  character +'.'+str(j+1)
        event.widget.config(bg='red')
        proper = 0
    elif(steps%2 == 1):
        character=chr(i+65)
        turn_end = character +'.'+str(j+1)
        event.widget.config(bg='red')
        #ask for appropiate range
        turn_start_x_str, turn_start_y = turn_start.split('.')
        turn_start_x = ord(turn_start_x_str)-65
        thisrange = abs(int(turn_start_x)-i)+abs(int(turn_start_y)-1-j)
        turn_range = str(thisrange)
        thisturn = turn_start + ':' + turn_range + ':' + turn_end
        proper = rb.conquer(board,thisturn, thiscolour,1) 
        if(proper == 1):
            #repaint
            thiscolour = rb.switch_colour(thiscolour)
        print("end of turn")
        repaint_gui_ai(board,colour,proper,policy) # repaint twice, now with waiting for remote turns
    thisturn = turn_start + ':' + turn_range + ':' + turn_end
    steps = steps+1
    return;  

def repaint_gui_ai(board,colour,proper,policy):
    global list_bg_label
    global list_label
    u = 0
    v = 0
    if(colour == 'E' and proper == 1): #Client side, first draw then send
        thiscolour = 'U'
    elif(colour == 'U' and proper == 1):#server side: open up new socket
        next_turn = get_next_turn_probabilistic(board,10,'E',policy);
        rb.conquer(board,next_turn,'E',1)
        print(next_turn)
        print("next turn executed")
        thiscolour = 'E'
    for i,row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour,piece_type,piece_value_str,piece_count = board[i,j].split('.')
            if(piece_type == 'P' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackpyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackcirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacktriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacksquare.gif').read())
            elif(piece_type == 'P' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitepyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitecirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitetriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitesquare.gif').read())
            else:
                bg_image=tk.PhotoImage(data=open('img/white.gif').read())
            bg_label = list_bg_label[u]
            L = list_label[v]
            u = u+1
            v = v+1
            bg_label.configure(image=bg_image, bg='white')
            bg_label.image=bg_image
            if(piece_value_str != '0'):
                L.configure(text=piece_value_str,anchor='s')
            else:
                L.configure(text='',anchor='s')
    #when the board is drawn display possible turns in terminal
    if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                if(thiscolour == 'E'):
                    turns = rb.getallturns(board, 'U') 
                    print(turns);
                    print(len(turns));
                elif(thiscolour == 'U'):
                    turns = rb.getallturns(board, 'E') 
                    print(turns);
                    print(len(turns));
    return;

def guiboard_ai(board,colour,proper,policy):
    global root
    global steps
    global first_turn
    global thiscolour
    global list_label
    global list_bg_label
    if(colour == 'E' and proper == 1): #Client side, first draw then send
        thiscolour = 'U'
    elif(colour == 'U'):#server side: open up new socket
        next_turn = get_next_turn_probabilistic(board,10,'E',policy);
        print(next_turn)
        rb.conquer(board,next_turn,'E',1)
        print("next turn executed")
        thiscolour = 'E'    
    #build board    
    for i,row in enumerate(board):
        for j,column in enumerate(row):
            piece_colour,piece_type,piece_value_str,piece_count = board[i,j].split('.')
            if(piece_type == 'P' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackpyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blackcirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacktriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'U'):
                bg_image=tk.PhotoImage(data=open('img/blacksquare.gif').read())
            elif(piece_type == 'P' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitepyr.gif').read())
            elif(piece_type == '2' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitecirc.gif').read())
            elif(piece_type == '3' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitetriangle.gif').read())
            elif(piece_type == '4' and piece_colour == 'E'):
                bg_image=tk.PhotoImage(data=open('img/whitesquare.gif').read())
            else:
                bg_image=tk.PhotoImage(data=open('img/white.gif').read())
            bg_label = tk.Label(root, image=bg_image)
            bg_label.image=bg_image
            bg_label.place(x=i,y=j,relwidth=1, relheight=1)
            bg_label.grid(row=i,column=j,padx='1',pady='1')
            list_bg_label.append(bg_label)
            if(piece_value_str != '0'):
                L = tk.Label(root,text=piece_value_str,anchor='s')
            else:
                L = tk.Label(root,text='',anchor='s')
            L.grid(row=i,column=j,padx='1',pady='1')
            list_label.append(L)
            bg_label.bind('<Button-1>',lambda e, i=i,j=j,k=board,m=colour,p=proper,l=policy: on_click_ai(i,j,e,k,m,p,l))
            borderpiece_lower = tk.Label(root, text=str(j+1))
            borderpiece_lower.grid(row=i+1,column=j,padx='1',pady='1')
        borderpiece = tk.Label(root, text=chr(i+65))
        borderpiece.grid(row=i,column=j+1,padx='1',pady='1')
    #when the board is drawn display possible turns in terminal
    if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                if(thiscolour == 'E'):
                    turns = rb.getallturns(board, 'U') 
                    print(turns);
                    print(len(turns));
                elif(thiscolour == 'U'):
                    turns = rb.getallturns(board, 'E') 
                    print(turns);
                    print(len(turns));
    return;


def play_against_ai(board):
    global gui
    global root
    end = 1
    even_turn = 1
    playing_colour = 'E'
    if('--gui' in sys.argv or '-gui' in sys.argv):
        gui = 1
        root = tk.Tk()
        guiboard_ai(board,'U',0,'destructive')
        tkMessageBox.showinfo("Information", " Willkommen zu einer Partie Rhithomachie. Weiss beginnt. Einfach auf die entsprechenden Felder klicken.")
        root.mainloop()
    else:
        while(end == 1):
            if('-l' in sys.argv or '--landscape' in sys.argv):
                rb.printboard_landscape(board)
            else:
                rb.printboard(board)
            if(even_turn == 1):
                print("AI is playing.");
                turn = get_next_turn_probabilistic(board,15,'E','destructive');
                even_turn = 0;
                playing_colour = 'E';
            elif(even_turn == 0):
                print("UNEVEN is playing. State your turn (<start_position>:<path length>:<end_position>. To end the game, simply type 'finish' or 'q'");
                even_turn = 1;
                playing_colour = 'U';
            if('-ai' in sys.argv or '--ai' in sys.argv):
                print("list of possible turns:");
                board_copy = numpy.copy(board);
                turns = rb.getallturns(board_copy, playing_colour); 
                print(turns);
                print(len(turns));
            if(even_turn == 1): #only in case of human interaction take turn
                turn = str(raw_input("Your turn:"));
                if(turn == 'finish' or turn == 'q'):
                    break;
            if(rb.conquer(board,turn, playing_colour,1) == 0): #in case of wrong turn: stay by colour
                even_turn = abs(even_turn-1)
    return;

def ai_create_log(board, filestring):
    end_max = 200
    end = end_max
    count_max = 3000
    count = count_max
    
    even_turn = 1
    playing_colour = 'E' 
    while (count > 0):
        board_played = numpy.copy(board);
        end = end_max;
        with open(filestring, "a") as logfile:
            logfile.write("Game Number: %s\n" % (count_max - count))
        while(end > 0):
            if(even_turn == 1):
                possible_turns = rb.getallturns(board_played,'E');
                turn = random.choice(possible_turns)
                #turn = get_next_turn_probabilistic(board,15,'E','defensive');
                even_turn = 0;
                playing_colour = 'E';
            elif(even_turn == 0):
                possible_turns = rb.getallturns(board_played,'U');
                turn = random.choice(possible_turns)
                #turn = get_next_turn_probabilistic(board,15,'U','defensive');
                even_turn = 1;
                playing_colour = 'U';
            rb.conquer(board_played,turn, playing_colour,1) ;
            with open(filestring, "a") as logfile:
                logfile.write("Playing Color: %s Turn: %s, count_even: %s count_uneven: %s\n" % (playing_colour,turn, rb.get_points(board_played,'E'), rb.get_points(board_played,'U')))
            end=end-1;
        count=count-1;    
    return;             
            
#turn = get_next_turn_probabilistic(board,10,'E','defensive')
#turn = get_next_turn(board,2,'E','destructive')
#print(turn)

#turn_list_test=['F.13:2:F.11','F.4:7:F.11']
turn_list_test=['E.13:002:E.11','C.4:2:D.5','G.14:003:G.11','D.5:2:E.6','D.13:002:D.11','F.4:2:F.6','F.13:0007:F.6','E.6:1:F.6','C.13:002:C.11','E.4:2:E.6','F.14:002:F.12','G.3:3:F.5','F.12:002:F.10','F.5:3:H.4','H.14:003:H.11','F.6:2:F.8','F.10:0002:F.8','E.6:2:E.8']

if('--replay' in sys.argv):
    argindex = sys.argv.index('--replay')
    filestring = sys.argv[argindex+1]
    simulate_logfile(board,filestring)  
    
if('-prev' in sys.argv):
    argindex = sys.argv.index('-prev')
    filestring = sys.argv[argindex+1]
    start_after_logfile(board,filestring)  
    
if('-ai_even' in sys.argv):
    play_against_ai(board)


if('-ai_log' in sys.argv):
    argindex = sys.argv.index('-ai_log')
    filename = sys.argv[argindex+1]
    ai_create_log(board, filename)
#even_turns = rb.getallturns(board,'E')
#print(even_turns);
#board_now = rb.board_after(board,turn_list_test)
#rb.board = board_now
#rb.play(board)
#print("even points:")
#print(rb.get_points(board,'E'))
#print("uneven points:")
#print(rb.get_points(board,'U'))